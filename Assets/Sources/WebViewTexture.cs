﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Runtime.InteropServices;

public class WebViewTexture : MonoBehaviour
{
	private RawImage m_WebView;
	private bool m_WebViewEnable;

#if UNITY_ANDROID
	private AndroidJavaClass m_NativePlugin;
#elif UNITY_IPHONE
	[DllImport("__Internal")]
	private static extern IntPtr _WebViewPlugin_Init(string gameObject);
	[DllImport("__Internal")]
	private static extern int _WebViewPlugin_Destroy(IntPtr instance);
	[DllImport("__Internal")]
	private static extern void _WebViewPlugin_SetMargins(IntPtr instance, int left, int top, int right, int bottom);
	[DllImport("__Internal")]
	private static extern void _WebViewPlugin_SetVisibility(IntPtr instance, bool visibility);
	[DllImport("__Internal")]
	private static extern void _WebViewPlugin_LoadURL(IntPtr instance, string url);
	[DllImport("__Internal")]
	private static extern void _WebViewPlugin_EvaluateJS(IntPtr instance, string url);
	[DllImport("__Internal")]
	private static extern void _WebViewPlugin_SetFrame(IntPtr instance, int x, int y, int width, int height);
	[DllImport("__Internal")]
	private static extern void _WebViewPlugin_GetPixel(IntPtr instance, ref IntPtr data);

	private IntPtr webView;
	private int m_width, m_height;
#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
    [DllImport("WebView")]
    private static extern IntPtr _CWebViewPlugin_Init(string gameObject, bool transparent, int width, int height, string ua, bool ineditor);
    [DllImport("WebView")]
    private static extern int _CWebViewPlugin_Destroy(IntPtr instance);
    [DllImport("WebView")]
    private static extern void _CWebViewPlugin_SetVisibility(IntPtr instance, bool visibility);
    [DllImport("WebView")]
    private static extern void _CWebViewPlugin_LoadURL(IntPtr instance, string url);
	[DllImport("WebView")]
    private static extern void _CWebViewPlugin_EvaluateJS(IntPtr instance, string url);
	[DllImport("WebView")]
	private static extern void _CWebViewPlugin_SetRect(IntPtr instance, int width, int height);
	[DllImport("WebView")]
	private static extern void _CWebViewPlugin_GetPixel(IntPtr instance, ref IntPtr data);
    [DllImport("WebView")]
    private static extern void _CWebViewPlugin_Update(IntPtr instance, int x, int y, float deltaY, bool down, bool press, bool release, bool keyPress, short keyCode, string keyChars, bool refreshBitmap);

	private IntPtr webView;
	private int m_width, m_height;
#endif


	private void Start()
	{
		string url = "https://yahoo.co.jp";
		int posX = 0;
		int posY = 320;
		int texWidth = 1024;
		int texHeight = 1024;
		var aspect = 1136.0f / Screen.width;

		m_WebViewEnable = true;
#if UNITY_ANDROID
		Init((Screen.width / 2) + (int)(posX / aspect), (Screen.height / 2) - (int)(posY / aspect), texWidth, texWidth, texWidth / 2, texHeight / 2);
		var texture2D = new Texture2D(texWidth / 2, texHeight / 2, TextureFormat.ARGB32, false);
/*
		var ptr = (IntPtr)m_WebTexture.GetWebTexture();
		var nativeTexture = Texture2D.CreateExternalTexture(texWidth, texHeight, TextureFormat.ARGB32, false, false, ptr);
		texture2D.UpdateExternalTexture(nativeTexture.GetNativeTexturePtr());
*/
		m_WebView = GameObject.Find("RenderCanvas/WebView").GetComponent<RawImage>();
		m_WebView.texture = texture2D;
		var rt = m_WebView.GetComponent<RectTransform>();
		rt.sizeDelta = new Vector2(texWidth * aspect, texHeight * aspect);
		rt.localPosition = new Vector3(posX, posY, 0);
		LoadUrl(url);
#elif UNITY_IPHONE
		int rw = (int)(texWidth / aspect);
		int rh = (int)(texWidth / aspect);
		rw = (rw + 3) & ~3;
		rh = (rh + 3) & ~3;
		Init((int)(posX / aspect), (int)(posY / aspect), rw, rh, texWidth / 2, texHeight / 2);
		var texture2D = new Texture2D(rw, rh, TextureFormat.ARGB32, false);

		m_WebView = GameObject.Find("RenderCanvas/WebView").GetComponent<RawImage>();
		m_WebView.texture = texture2D;
		var rt = m_WebView.GetComponent<RectTransform>();
		rt.sizeDelta = new Vector2(texWidth, texHeight);
		rt.localPosition = new Vector3(posX, posY, 0);
		LoadUrl(url);
		SetVisible(true);
#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
		int rw = (int)(texWidth / aspect);
		int rh = (int)(texWidth / aspect);
		rw = (rw + 3) & ~3;
		rh = (rh + 3) & ~3;
		Init((int)(posX / aspect), (int)(posY / aspect), rw, rh, texWidth / 2, texHeight / 2);
		var texture2D = new Texture2D(rw, rh, TextureFormat.ARGB32, false);

		m_WebView = GameObject.Find("RenderCanvas/WebView").GetComponent<RawImage>();
		m_WebView.texture = texture2D;
		var rt = m_WebView.GetComponent<RectTransform>();
		rt.sizeDelta = new Vector2(texWidth, texHeight);
		rt.localPosition = new Vector3(posX, posY, 0);
		LoadUrl(url);
		SetVisible(true);
#endif
	}

	protected virtual void OnDestroy()
	{
#if UNITY_ANDROID
        if (m_NativePlugin == null)
            return;
        m_NativePlugin.Call("Destroy");
		m_NativePlugin = null;
#elif UNITY_IPHONE
        if (webView == IntPtr.Zero)
            return;
        _WebViewPlugin_Destroy(webView);
		webView = IntPtr.Zero;
#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
        if (webView == IntPtr.Zero)
            return;
        _CWebViewPlugin_Destroy(webView);
		webView = IntPtr.Zero;
#endif
	}

	private void Update()
	{
		if (!m_WebViewEnable)
		{
			return;
		}
#if UNITY_ANDROID
#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
        if (webView != IntPtr.Zero)
		{
			Vector3 pos = Input.mousePosition;
	        bool down = Input.GetButton("Fire1");
	        bool press = Input.GetButtonDown("Fire1");
	        bool release = Input.GetButtonUp("Fire1");
			float deltaY = Input.GetAxis("Mouse ScrollWheel");
			var aspect = 1136.0f / Screen.width;
			var posX = ((pos.x  - (Screen.width * 0.5f)) * aspect) - (m_WebView.rectTransform.anchoredPosition.x - m_WebView.rectTransform.sizeDelta.x * m_WebView.rectTransform.pivot.x);
			Debug.Log(string.Format("{0}, {1}, {2}", pos.x, posX, deltaY));
			_CWebViewPlugin_Update(webView, (int)(posX + m_WebView.rectTransform.sizeDelta.x * 0.5f), (int)pos.y + 1024, deltaY, down, press, release, false, 0, "", true);
		}
#endif

		var data = GetWebTexturePixel();
		if (data != null)
		{
			var tex = (Texture2D)m_WebView.texture;
			tex.LoadRawTextureData(data);
			tex.Apply();
		}
	}

	public void Init(int posX, int posY, int width, int height, int texWidth, int texHeight)
	{
#if UNITY_ANDROID
		if (!Application.isEditor)
		{
			m_NativePlugin = new AndroidJavaClass("jp.ne.pickle.libwebtexture.UnityConnect");
			m_NativePlugin.CallStatic("initialize", posX, posY, width, height, texWidth, texHeight);
		}
#elif UNITY_IPHONE
		m_width = width;
		m_height = height;
		webView = _WebViewPlugin_Init("name");
		SetPosition(posX, posY);
#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
		m_width = width;
		m_height = height;
		webView = _CWebViewPlugin_Init("name", false, width, height, "", Application.platform == RuntimePlatform.OSXEditor);
		SetPosition(posX, posY);
#endif
	}

	public void SetPosition(int posX, int posY)
	{
#if UNITY_ANDROID
		if (!Application.isEditor)
		{
			m_NativePlugin.CallStatic("setPosition", posX, posY);
		}
#elif UNITY_IPHONE
		if (webView == IntPtr.Zero) return;
		_WebViewPlugin_SetFrame(webView, posX, posY, m_width, m_height);
#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
		if (webView == IntPtr.Zero) return;
		_CWebViewPlugin_SetRect(webView, m_width, m_height);
#endif
	}

	public void SetWebTexture(int textureId, int width, int height)
	{
#if UNITY_ANDROID
		if (!Application.isEditor)
		{
			m_NativePlugin.CallStatic("setTetxure", textureId, width, height);
		}
#elif UNITY_IPHONE
#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
#endif
	}

	public int GetWebTexture()
	{
#if UNITY_ANDROID
		if (!Application.isEditor)
		{
			return m_NativePlugin.CallStatic<int>("getTetxure");
		}
		return 0;
#else
		return 0;
#endif
	}

	public byte[] GetWebTexturePixel()
	{
#if UNITY_ANDROID
		if (!Application.isEditor)
		{
			return m_NativePlugin.CallStatic<byte[]>("getPixel");
		}
		return null;
#elif UNITY_IPHONE
		if (webView == IntPtr.Zero) return null;
		byte[] data = new byte[m_width * m_height * 4];
		var handle = default(GCHandle);
		try
		{
			handle = GCHandle.Alloc(data, GCHandleType.Pinned);
			var ptr = handle.AddrOfPinnedObject();
			_WebViewPlugin_GetPixel(webView, ref ptr);
		}
		finally
		{
			if (handle != default(GCHandle))
				handle.Free();
		}

		return data;
#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
		if (webView == IntPtr.Zero) return null;
		byte[] data = new byte[m_width * m_height * 4];
		var handle = default(GCHandle);
		try
		{
			handle = GCHandle.Alloc(data, GCHandleType.Pinned);
			var ptr = handle.AddrOfPinnedObject();
			_CWebViewPlugin_GetPixel(webView, ref ptr);
		}
		finally
		{
			if (handle != default(GCHandle))
				handle.Free();
		}

		return data;
#else
		return null;
#endif
	}

	public void LoadUrl(string url)
	{
#if UNITY_ANDROID
		if (!Application.isEditor)
		{
			m_NativePlugin.CallStatic("loadUrl", url);
		}
#elif UNITY_IPHONE
		if (webView == IntPtr.Zero)
		{
			return;
		}
		_WebViewPlugin_LoadURL(webView, url);
#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
		if (webView == IntPtr.Zero)
		{
			return;
		}
		_CWebViewPlugin_LoadURL(webView, url);
#endif
	}
	
	public void SetVisible(bool visible)
	{
		m_WebViewEnable = visible;
#if UNITY_ANDROID
		if (!Application.isEditor)
		{
			m_NativePlugin.CallStatic("setVisible", visible);
		}
#elif UNITY_IPHONE
		if (webView == IntPtr.Zero)
		{
			return;
		}
		_WebViewPlugin_SetVisibility(webView, visible);
#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
		if (webView == IntPtr.Zero)
		{
			return;
		}
		_CWebViewPlugin_SetVisibility(webView, visible);
#endif
	}
}
