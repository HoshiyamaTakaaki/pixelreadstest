using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Main : MonoBehaviour
{
	private Transform m_Camera;
	private Transform m_Target;
	private float m_Rotate1;
	private float m_Rotate2;

	private Text m_FpsText;
	private float m_FpsTimer;
	private int m_FpsCounter;

	private bool[] m_Enable;

	public void OnPushButton(int num)
	{
		m_Enable[num - 1] = !m_Enable[num - 1];
		var text = GameObject.Find("RenderCanvas/Button" + num + "/Text").GetComponent<Text>();
		text.text = m_Enable[num - 1] ? "ON" : "OFF";
		if (num == 1)
		{
			m_Camera.gameObject.SetActive(m_Enable[num - 1]);
		}
		else if (num == 2)
		{
			var scr = m_Camera.GetComponent<ReadPixels>();
			scr.SetEnable(m_Enable[num - 1]);
		}
		else if (num == 3)
		{
			var scr = m_Camera.GetComponent<ReadPixels>();
			if (m_Enable[num - 1])
			{
				var movieFileName = "SampleMovie" + System.DateTime.Now.ToString("yyyyMMddHHmm") + ".mp4";
				var movieFilePath = Path.Combine(Application.temporaryCachePath, "Movie");
				scr.StartRecord(movieFilePath, movieFileName);
			}
			else
			{
				scr.StopRecord();
			}
		}
		else if (num == 4)
		{
			var scr = m_Camera.GetComponent<WebViewTexture>();
			scr.SetVisible(m_Enable[num - 1]);
		}
	}

	// Use this for initialization
	void Start()
	{
		var renderCamera = GameObject.Find("RenderCamera");
		m_Camera = renderCamera.transform;
		m_Target = GameObject.Find("Cube1").transform;
		m_FpsText = GameObject.Find("RenderCanvas/Text").GetComponent<Text>();

		GameObject.Find("Cube1").GetComponent<Renderer>().material.SetColor("_Color", Color.red);
		GameObject.Find("Cube2").GetComponent<Renderer>().material.SetColor("_Color", Color.blue);
		GameObject.Find("Cube3").GetComponent<Renderer>().material.SetColor("_Color", Color.yellow);
		GameObject.Find("Cube4").GetComponent<Renderer>().material.SetColor("_Color", Color.green);

		m_Rotate1 = 0;
		m_Rotate2 = 0;
		m_FpsTimer = 0;
		m_FpsCounter = 0;

		m_Enable = new bool[4];
		m_Enable[0] = true;
		m_Enable[1] = true;
		m_Enable[2] = false;
		m_Enable[3] = true;
		Application.targetFrameRate = 60;
	}
	
	// Update is called once per frame
	void Update ()
	{
		m_FpsTimer += Time.deltaTime;
		if (m_FpsTimer >= 1.0f)
		{
			m_FpsText.text = "FPS:" + m_FpsCounter;
			m_FpsTimer -= 1.0f;
			m_FpsCounter = 0;
		}
		m_FpsCounter++;


		m_Rotate1 += Time.deltaTime * 90;
		m_Rotate1 %= 360;
		m_Rotate2 += Time.deltaTime * 45;
		m_Rotate2 %= 360;
		Vector3 pos = Vector3.back * 7;
		pos = Quaternion.Euler(m_Rotate2, m_Rotate1, 0) * pos;
		m_Camera.localPosition = pos;
		m_Camera.LookAt(m_Target);
	}
}
