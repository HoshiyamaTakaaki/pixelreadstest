#import "MovieRecorder.h"
#import <AVFoundation/AVFoundation.h>
#import <Photos/Photos.h>

@interface MovieRecorderPlugin : NSObject<MovieRecorderDelegate>
{
	MovieRecorder* session;
	dispatch_queue_t _callbackQueue;
	double mPresentTimeUs;
	int mAudioChannel;
	bool mRecording;
}
@end

@implementation MovieRecorderPlugin

- (bool)isRecording
{
	return mRecording;
}

- (void)movieRecorderDidFinishPreparing:(MovieRecorder *)recorder
{
	NSLog(@"movieRecorderDidFinishPreparing");
	mRecording = true;
	mPresentTimeUs = [[NSDate date] timeIntervalSince1970];
}

- (void)movieRecorder:(MovieRecorder *)recorder didFailWithError:(NSError *)error
{
	NSLog(@"didFailWithError:%@", error);
	[self close];
}

- (void)movieRecorderDidFinishRecording:(MovieRecorder *)recorder
{
	NSLog(@"movieRecorderDidFinishRecording");
	[self close];
}


- (id)init
{
	self = [super init];

	session = nil;
	_callbackQueue = nil;
	mRecording = false;
	mAudioChannel = 0;

	return self;
}

- (void)close
{
	mRecording = false;

	session = nil;

	_callbackQueue = nil;
}

- (void)startRecord:(const char*)fileName width:(int)width height:(int)height audioChannel:(int)channel audioSampleRate:(int)samplerate
{
	_callbackQueue = dispatch_queue_create( "ne.jp.pickle.movierecorder.recordercallback", DISPATCH_QUEUE_SERIAL );
	NSURL* url = [[NSURL alloc] initFileURLWithPath:[NSString stringWithCString:fileName encoding:NSUTF8StringEncoding]];
	session = [[MovieRecorder alloc] initWithURL:url delegate:self callbackQueue:_callbackQueue];

	NSDictionary* vsettings = [NSDictionary dictionaryWithObjectsAndKeys:
											AVVideoCodecH264, AVVideoCodecKey,
											@(width), AVVideoWidthKey,
											@(height), AVVideoHeightKey,
											nil];
	CMVideoFormatDescriptionRef videoFormatDescription = NULL;
	CMVideoFormatDescriptionCreate(kCFAllocatorDefault, kCMVideoCodecType_H264, width, height, NULL, &videoFormatDescription);
	[session addVideoTrackWithSourceFormatDescription:videoFormatDescription transform:CGAffineTransformIdentity settings:nil usePixelBuffer:YES];

	mAudioChannel = channel;
	if (channel > 0)
	{
		AudioChannelLayout channelLayout;
		memset(&channelLayout, 0, sizeof(AudioChannelLayout));
		channelLayout.mChannelLayoutTag = kAudioChannelLayoutTag_Stereo;
		NSDictionary* asettings = [NSDictionary dictionaryWithObjectsAndKeys:
												[NSNumber numberWithInt:kAudioFormatMPEG4AAC], AVFormatIDKey,
												[NSNumber numberWithFloat:samplerate], AVSampleRateKey,
												[NSNumber numberWithInt:48000], AVEncoderBitRatePerChannelKey,
												[NSNumber numberWithInt:channel], AVNumberOfChannelsKey,
												[NSData dataWithBytes:&channelLayout length:sizeof(AudioChannelLayout)], AVChannelLayoutKey,
												nil];
		CMAudioFormatDescriptionRef audioFormatDescription = NULL;
		AudioStreamBasicDescription asbd;
		asbd.mSampleRate = samplerate;
		asbd.mFormatID = kAudioFormatLinearPCM;
		asbd.mFormatFlags = kAudioFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked;
		asbd.mFramesPerPacket = 1;
		asbd.mChannelsPerFrame = channel;
		asbd.mBitsPerChannel = 16;
		asbd.mBytesPerFrame = (asbd.mBitsPerChannel / 8) * asbd.mChannelsPerFrame;
		asbd.mBytesPerPacket = asbd.mBytesPerFrame * asbd.mFramesPerPacket;
		asbd.mReserved = 0;
		CMAudioFormatDescriptionCreate(kCFAllocatorDefault, &asbd, 0, NULL, 0, NULL, NULL, &audioFormatDescription);
		[session addAudioTrackWithSourceFormatDescription:audioFormatDescription settings:asettings];
	}

	[session prepareToRecord];
}

- (void)stopRecord
{
	[session finishRecording];
	mRecording = false;
}

void ReleaseCVPixelBufferForCVPixelBufferCreateWithBytes(void *releaseRefCon, const void *baseAddr)
{
	CFDataRef bufferData = (CFDataRef)releaseRefCon;
	CFRelease(bufferData);
}

- (void)pushVideo:(void*)data width:(int)w height:(int)h
{
	if (!mRecording)
	{
		return;
	}

	auto size = w * h * 4;
	CFDataRef bufferData = CFDataCreate(kCFAllocatorDefault, (UInt8*)data, size);
	unsigned int* pixelData = (unsigned int*)CFDataGetBytePtr(bufferData);
	unsigned int* backPixelData = pixelData + ((h - 1) * w);
	for (int y = 0; y < (h >> 1); y++)
	{
		for (int x = 0; x < w; x++)
		{
			auto col0 = *pixelData;
			auto col1 = *backPixelData;
			col0 = (col0 & 0xff00ff00) | ((col0 & 0x00ff0000) >> 16) | ((col0 & 0x000000ff) << 16);
			col1 = (col1 & 0xff00ff00) | ((col1 & 0x00ff0000) >> 16) | ((col1 & 0x000000ff) << 16);
			*backPixelData = col0;
			*pixelData = col1;
			pixelData++;
			backPixelData++;
		}
		backPixelData -= (w << 1);
	}

	CVPixelBufferRef pixel = NULL;
	CVPixelBufferCreateWithBytes(kCFAllocatorDefault, w, h, kCVPixelFormatType_32BGRA, (void*)CFDataGetBytePtr(bufferData), w * 4, ReleaseCVPixelBufferForCVPixelBufferCreateWithBytes, (void*)bufferData, nil, &pixel);
	int pts = (int)(([[NSDate date] timeIntervalSince1970] - mPresentTimeUs) * 1000000) + (50 * 1000);
	[session appendVideoPixelBuffer:pixel withPresentationTime:CMTimeMake(pts, 1000000)];
	CFRelease(pixel);
}

- (void)pushAudio:(void*)data size:(int)dataSize
{
	if (!mRecording || mAudioChannel == 0)
	{
		return;
	}
	int pts = (int)(([[NSDate date] timeIntervalSince1970] - mPresentTimeUs) * 1000000);
	[session appendAudioSampleBuffer:data dataSize:dataSize withPresentationTime:CMTimeMake(pts, 1000000)];
}
@end

extern "C" {
	void*	_MovieRecorderPlugin_Init();
	void	_MovieRecorderPlugin_Destroy(void* instance);
	void	_MovieRecorderPlugin_StartRecord(void* instance, const char* fileName, int width, int height);
	void	_MovieRecorderPlugin_StopRecord(void* instance);
	void	_MovieRecorderPlugin_PushVideo(void* instance, char* data, int width, int height);
	void	_MovieRecorderPlugin_PushAudio(void* instance, char* data, int datasize);

	int		_GetStorageFreeSpace();

	void	_RequestCameraRollPermission(const char* gameObjectName, const char* callbackMethodName);
	int		_HasCameraRollPermission();
	void	_creationRequestForAssetFromVideoAtFileURL(const char* url, const char* gameObjectName, const char* callbackMethodName);

	void	_RequestRecordPermission(const char* gameObjectName, const char* callbackMethodName);
	int		_HasRecordPermission();
}

int _GetStorageFreeSpace()
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
	NSDictionary *dic = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error:nil];

	int free = 0;
	if (dic)
	{
		float MB = 1024.0 * 1024.0; //メガバイトで計算
		free = (int)((unsigned long long)[dic[@"NSFileSystemFreeSize"] unsignedLongLongValue] / (1024 * 1024));
	}

	return free;
}

void _RequestCameraRollPermission(const char* gameObjectName, const char* callbackMethodName)
{
	NSString* gobj = [NSString stringWithUTF8String:gameObjectName];
	NSString* meth = [NSString stringWithUTF8String:callbackMethodName];
	[PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
		if (gameObjectName)
		{
			UnitySendMessage([gobj UTF8String], [meth UTF8String], [[NSString stringWithFormat:@"%d", (int)status] UTF8String]);
		}
	}];
}

int _HasCameraRollPermission()
{
	PHAuthorizationStatus authStatus = [PHPhotoLibrary authorizationStatus];
	return (int)authStatus;
}

void _creationRequestForAssetFromVideoAtFileURL(const char* url, const char* gameObjectName, const char* callbackMethodName)
{
	NSURL* furl = [NSURL URLWithString:[NSString stringWithUTF8String:url]];
	NSString* gobj = [NSString stringWithUTF8String:gameObjectName];
	NSString* meth = [NSString stringWithUTF8String:callbackMethodName];
	[[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
		// In iOS 9 and later, it's possible to move the file into the photo library without duplicating the file data.
		// This avoids using double the disk space during save, which can make a difference on devices with limited free disk space.
		if ( [PHAssetResourceCreationOptions class] ) {
			PHAssetResourceCreationOptions *options = [[PHAssetResourceCreationOptions alloc] init];
			options.shouldMoveFile = NO;
			PHAssetCreationRequest *changeRequest = [PHAssetCreationRequest creationRequestForAsset];
			[changeRequest addResourceWithType:PHAssetResourceTypeVideo fileURL:furl options:options];
		}
		else
		{
			[PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:furl];
		}
	} completionHandler:^(BOOL success, NSError *error) {
		// 完了後の処理
		if (gameObjectName)
		{
			UnitySendMessage([gobj UTF8String], [meth UTF8String], [[NSString stringWithFormat:@"%d", (int)success] UTF8String]);
		}
	}];
}

void _RequestRecordPermission(const char* gameObjectName, const char* callbackMethodName)
{
	NSString* gobj = [NSString stringWithUTF8String:gameObjectName];
	NSString* meth = [NSString stringWithUTF8String:callbackMethodName];
	[[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
		if (gameObjectName)
		{
			UnitySendMessage([gobj UTF8String], [meth UTF8String], [[NSString stringWithFormat:@"%d", (int)granted] UTF8String]);
		}
	}];
}

int _HasRecordPermission()
{
	return (int)[[AVAudioSession sharedInstance] recordPermission];
}

void *_MovieRecorderPlugin_Init()
{
	return (__bridge_retained void *)[[MovieRecorderPlugin alloc] init];
}

void _MovieRecorderPlugin_Destroy(void *instance)
{
	MovieRecorderPlugin* plugin = (__bridge MovieRecorderPlugin*)instance;
	[plugin close];
}

void _MovieRecorderPlugin_StartRecord(void* instance, const char* fileName, int width, int height)
{
	MovieRecorderPlugin* plugin = (__bridge MovieRecorderPlugin*)instance;
	[plugin startRecord:fileName width:width height:height audioChannel:2 audioSampleRate:44100];
}

void _MovieRecorderPlugin_StopRecord(void* instance)
{
	MovieRecorderPlugin* plugin = (__bridge MovieRecorderPlugin*)instance;
	[plugin stopRecord];
}

void _MovieRecorderPlugin_PushVideo(void* instance, char* data, int width, int height)
{
	MovieRecorderPlugin* plugin = (__bridge MovieRecorderPlugin*)instance;
	[plugin pushVideo:data width:width height:height];
}

void _MovieRecorderPlugin_PushAudio(void* instance, char* data, int datasize)
{
	MovieRecorderPlugin* plugin = (__bridge MovieRecorderPlugin*)instance;
	[plugin pushAudio:data size:datasize];
}
