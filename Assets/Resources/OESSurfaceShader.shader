﻿Shader "Custom/OESSurfaceShader"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="false"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
		GLSLPROGRAM
			#extension GL_OES_EGL_image_external : require
			uniform samplerExternalOES _MainTex;
			uniform vec4 _Color;

			#ifdef VERTEX
			varying vec4 textureCoordinates;

			void main() {
				textureCoordinates = gl_MultiTexCoord0;
			    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
			}
			#endif

			#ifdef FRAGMENT
			varying vec4 textureCoordinates;

			void main() {
			    gl_FragColor = texture2D(_MainTex, vec2(textureCoordinates)) * _Color;
			}
			#endif
		ENDGLSL
		}
	}

	//Fallback "Transparent/VertexLit"
}
